import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { ProjectsComponent } from './modules/projects/projects.component';
import { TasksComponent } from './modules/tasks/tasks.component';
import { LogInComponent } from './modules/log-in/log-in.component';
import { AuthGuard } from './auth.guard';
import { UsersComponent } from './modules/users/users.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'proyectos',
        component: ProjectsComponent,
      },
      {
        path: 'tareas',
        component: TasksComponent,
      },
      {
        path: 'desarrolladores',
        component: UsersComponent,
      },
    ],
  },
  {
    path: 'login',
    component: LogInComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
