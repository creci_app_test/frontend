import { Component, OnInit } from '@angular/core';
import { IUser } from '../../interfaces';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  public user: IUser;

  constructor() {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.menuItems = [
      { path: '/', title: 'Inicio', icon: 'home' },
      { path: '/tareas', title: 'Tareas', icon: 'fact_check' },
      { path: '/proyectos', title: 'Proyectos', icon: 'assignment' },
      { path: '/desarrolladores', title: 'Desarrolladores', icon: 'people' },
    ];
  }

}
