export interface IUser{
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    rol: string;
    _id: string;
    error?: Error;
}

export interface ILogin {
    email: string;
    password: string;
    error?: Error;
}

export interface ITaks{
    error?: Error;
}

export interface IProject{
    error?: Error;
}
