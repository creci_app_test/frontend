import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  public projects: object;

  constructor(private projectService: ProjectService) {
    this.projectService.getAllProjects().subscribe( (data: any) => {
      this.projects = data.projects;
      console.log(this.projects);
    });
  }

  ngOnInit(): void {
  }

}
