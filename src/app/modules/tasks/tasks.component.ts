import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks: ArrayBuffer;

  constructor(private taskService: TaskService) {
    this.taskService.getAllTasks().subscribe( (data: any) => {
      if (data.OK) {
        this.tasks = data.tasks;
        console.log(this.tasks);
      } else {
        console.log(data.error);
      }
    });
  }

  ngOnInit(): void {
  }

}
