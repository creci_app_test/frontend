import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent {

  public loginForm  = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private authService: AuthService, private router: Router) { }

  async onSubmit() {
    this.authService.logIn(this.loginForm.value).subscribe(
      ({logedUser, token}) => {
        localStorage.setItem('user', JSON.stringify(logedUser));
        localStorage.setItem('token', token);
        this.router.navigate(['/']);
      },
      (error: any) => {
        Swal.fire({
          title: error.error,
          icon: 'error',
          showConfirmButton: true,
          heightAuto: false,
          confirmButtonColor: '#6c757d',
        });
      }
    );
  }
}
