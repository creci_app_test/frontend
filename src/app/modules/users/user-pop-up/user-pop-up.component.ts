import { Component, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';
import { IUser } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-user-pop-up',
  templateUrl: './user-pop-up.component.html',
  styleUrls: ['./user-pop-up.component.css']
})
export class UserPopUpComponent{

  public parameters: any;
  public user = new FormGroup({
    _id: new FormControl(''),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', Validators.required),
    rol: new FormControl('Developer')
  });

  constructor(public dialogRef: MatDialogRef<UserPopUpComponent>, public userServices: UserService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.parameters = data[0];
    if (data[1]) {
      this.user.patchValue(data[1]);
    }
  }

  save(): void {
    if (this.parameters.create) {
      this.user.removeControl('_id');
      this.userServices.createUser(this.user.value).subscribe((user: IUser) => {
        if (user) {
          this.dialogRef.close({ user, action: 'created'});
        }
      });
    } else if (this.parameters.update) {
      this.userServices.updateUser(this.user.value).subscribe((user: IUser) => {
        if (user) {
          this.dialogRef.close({ user, action: 'updated'});
        }
      });
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
