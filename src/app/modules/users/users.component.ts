import {Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';
import { UserPopUpComponent } from './user-pop-up/user-pop-up.component';
import Swal from 'sweetalert2';
import { IUser } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public displayedColumns: string[] = ['firstName', 'lastName', 'email', 'acciones'];
  public dataSource: MatTableDataSource<IUser>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  private users: IUser[];

  constructor(private userService: UserService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.updateTable();
  }

  createUser(): void {
    const paramaters = {
      create: true,
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.disableClose = true;
    dialogConfig.data = [paramaters, null];
    const dialogRef = this.dialog.open(UserPopUpComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data: any) => {
      console.log(data);
      if (data) {
        if (data.action === 'created') {
          this.users = [...this.users, ...[data.user]];
        }

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Desarrollador creado',
          showConfirmButton: false,
          timer: 1500,
          heightAuto: false,
        });
      }
      this.updateTable();
    });
  }

  updateUser(user: IUser): void {
    const paramaters = {
      update: true,
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.disableClose = true;
    dialogConfig.data = [paramaters, user];
    const dialogRef = this.dialog.open(UserPopUpComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data: any) => {
      console.log(data);
      if (data) {
        if (data.action === 'created') {
          this.users = [...this.users, ...[data.user]];
        }

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Desarrollador actualizado',
          showConfirmButton: false,
          timer: 1500,
          heightAuto: false,
        });
      }
      this.updateTable();
    });
  }

  deleteUser(user: IUser): void {
    Swal.fire({
      position: 'center',
      text: 'Esta seguro de eliminar a ' + user.firstName + ' ' + user.lastName,
      icon: 'info',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'ELIMINAR',
      cancelButtonColor: '#6c757d',
      cancelButtonText: 'CANCELAR',
      showCancelButton: true,
      heightAuto: false,
    }).then(result => {
      if (result.value) {
        this.userService.deleteUser(user).subscribe((data: any) => {
          this.users = this.users.filter((userFiltered) => userFiltered._id !== data._id );
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Desarrollador eliminado',
            showConfirmButton: false,
            timer: 1500,
            heightAuto: false,
          });
          this.updateTable();
        });
      }
    });
  }

  updateTable(): void {
    this.userService.getAllUsers().subscribe( (users: IUser[]) => {
      if (users) {
        this.users = users.filter(user => user.rol !== 'Leader');
      }
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
