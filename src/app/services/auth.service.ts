import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ILogin } from '../shared/interfaces';
import { ResolveData } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = 'http://192.168.0.13:3000/api/v1.0/user/login';

  constructor(private http: HttpClient) {
  }

  logIn(loginForm: ILogin): ResolveData {
    return this.http.post(this.URL, loginForm).pipe(map((data: any) => {
      return data;
    }));
  }

  logOut(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  loggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  getToken(): string {
    if (localStorage.getItem('token')) {
      return localStorage.getItem('token');
    } else {
      return '';
    }
  }

}
