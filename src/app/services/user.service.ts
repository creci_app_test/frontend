import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public URL = 'http://192.168.0.13:3000/api/v1.0/user';

  constructor(private http: HttpClient) { }

  getAllUsers(){
    return this.http.get(this.URL);
  }

  createUser(user: IUser) {
    return this.http.post(this.URL, user);
  }

  deleteUser(user: IUser) {
    const { _id } = user;
    return this.http.delete(`${this.URL}/${_id}`);
  }

  updateUser(user: IUser) {
    const { _id } = user;
    return this.http.patch(`${this.URL}/${_id}`, user);
  }
}
