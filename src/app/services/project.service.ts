import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public URL = 'http://192.168.0.13:3000/api/v1.0/project';

  constructor(private http: HttpClient) { }

  getAllProjects(){
    return this.http.get(this.URL).pipe(map(data => {
      return data;
    }));
  }
}
